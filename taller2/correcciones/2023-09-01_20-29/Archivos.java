package aed;

import java.util.Scanner;
import java.io.PrintStream;

class Archivos {
    float[] leerVector(Scanner entrada, int largo) {
        float[] res = new float[largo];

        for(int i = 0; i<largo;i++){

            res[i] = entrada.nextFloat();

        }

        return res;
    }

    float[][] leerMatriz(Scanner entrada, int filas, int columnas) {
        float[][] matriz = new float[filas][columnas];

        for(int i = 0; i<filas;i++){
            for(int j = 0; j < columnas; j++){
                matriz[i][j] = entrada.nextFloat();
            }
        

    }
        return matriz;
    }

    void imprimirPiramide(PrintStream salida, int alto) {
        for(int i = 1; i<=alto;i++){
            for(int j = 1; j<=2*alto-1;j++){
                if((j>=1)&&(j<(alto-i+1))){
                    salida.print(" ");
                }
                else if((j>=(alto-i+1))&&(j<=(alto+i-1))){
                    salida.print("*");
                }
                if((j>(alto+i-1))&&(j<=(2*alto-1))){
                    salida.print(" ");
                }
            }
            salida.println();

        }
    }
}
